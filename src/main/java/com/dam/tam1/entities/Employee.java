package com.dam.tam1.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
@Getter
@Setter
@EqualsAndHashCode
public class Employee implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long employee_id;
	
	@NotNull
	@NotEmpty(message = "The first name cannot be empty")
	@Min(value = 5, message = "The first name cannot be less than 5 characters")
	@Max(value = 30, message = "The first name cannot be more than 30 characters")  
	private String firstName;
	
	@NotNull
	@NotEmpty(message = "The last name cannot be empty")
	@Min(value = 5, message = "The last name cannot be less than 5 characters")
	@Max(value = 30, message = "The last name cannot be more than 30 characters")  
	private String lastName;
    
	@NotNull
	@NotEmpty(message = "The address cannot be empty")
	private String address;
	
	@Email
	@NotEmpty(message = "The email address cannot be empty")
	@NotNull
	private String emailAddress;
	
    @Temporal(TemporalType.DATE)
	private Date dateOfBirth;
    
    @NotEmpty(message = "The phone number field cannot be empty")
	@NotNull
	@Min(value = 15, message = "The phone no. cannot be less than 15 characters")
	@Max(value = 20, message = "The phone no. cannot be more than 20 characters")
	private String phoneNo;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "position_id")
	private Position position;
	
}
