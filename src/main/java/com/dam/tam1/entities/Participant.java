package com.dam.tam1.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Participant extends Employee implements Serializable{
	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "The participant role cannot be empty")
	@NotNull
	@Min(value = 5, message = "The participant role name cannot be less than 5 characters")
	@Max(value = 20, message = "The participant role name cannot be more than 20 characters")
	private String participant_role;
	
	@OneToMany(mappedBy = "participant")
	@JsonManagedReference
	@JsonIgnore
	private List<InterviewParticipant> interviews;
	
	private String mentions;

	public Participant(Long employee_id, String firstName, String lastName, String address, String emailAddress,
			Date dateOfBirth, String phoneNo, Position position, String participant_role, String mentions) {
		super(employee_id, firstName, lastName, address, emailAddress, dateOfBirth, phoneNo, position);
		this.participant_role = participant_role;
		this.mentions = mentions;
	}

}
