package com.dam.tam1.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class InterviewParticipantPk implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "interview_id")
	private Long interviewId;
	
	@Column(name = "participant_id")
	private Long participantId;

	public InterviewParticipantPk() {}
	
	public InterviewParticipantPk(Long interviewId, Long participantId) {
		this.interviewId = interviewId;
		this.participantId = participantId;
	}

	public Long getInterviewId() {
		return interviewId;
	}

	public void setInterviewId(Long interviewId) {
		this.interviewId = interviewId;
	}

	public Long getParticipantId() {
		return participantId;
	}

	public void setParticipantId(Long participantId) {
		this.participantId = participantId;
	}


	
	
}
