package com.dam.tam1.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "interviews_participants")
public class InterviewParticipant implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private InterviewParticipantPk interviewParticipantPk;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "interview_id", insertable = false, updatable = false)
	@JsonBackReference(value = "interview_back")
	private Interview interview;
	

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "participant_id", insertable = false, updatable = false)
	@JsonBackReference(value = "participant_back")
	private Participant participant;
	
	@Temporal(TemporalType.DATE)
	private Date dateOfAllocation;

	public InterviewParticipant(InterviewParticipantPk interviewParticipantPk, Date dateOfAllocation) {
		super();
		this.interviewParticipantPk = interviewParticipantPk;
		this.dateOfAllocation = dateOfAllocation;
	}
	
	@Transient
	private Map<String, Object> additionalInformations = new HashMap<String, Object>();

	@JsonAnyGetter
	public Map<String, Object> getAdditionalInformation() {
		return additionalInformations;
	}

	public void setAdditionalInformation(Map<String, Object> additionalInformation) {
		this.additionalInformations = additionalInformation;
	}
	
	@JsonAnySetter 
	public void setAdditionalInformation(final String key, final Object value) { 
		this.additionalInformations.put(key, value); 
	}
}
