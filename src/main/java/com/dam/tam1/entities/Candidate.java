package com.dam.tam1.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Transactional(readOnly=true)
@JsonDeserialize
@Table(name="candidates")
public class Candidate implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long candidate_id;
	
	@NotEmpty(message = "The first name cannot be empty")
	@Min(value = 5, message = "The number of characters is less than 5 characters")
	@Max(value= 40, message = "The number of characters is more than 40")
	private String firstName;
	
	@NotEmpty(message = "The last name cannot be empty")
	@Min(value = 5, message = "The number of characters is less than 5 characters")
	@Max(value= 40, message = "The number of characters is more than 40")
	private String lastName;
	@NotEmpty(message = "The address cannot be empty")
	private String address;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern="dd-MM-yyyy")
	private Date dob;
	
	@Email(message = "Email should be valid")
	@NotEmpty(message = "The email address cannot be empty")
	private String emailAddress;
	private String phoneNo;
	
	@JsonManagedReference
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL,  mappedBy = "candidate")
	private List<Submission> submissions;
	
	
}
