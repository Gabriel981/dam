package com.dam.tam1.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.val;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Table(name="interviews")
@JsonIgnoreProperties(value={ "participants" }, allowGetters=true)
public class Interview implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long interview_id;
	
	@Temporal(TemporalType.DATE)
	private Date interviewDate;
	
	@Enumerated(EnumType.STRING)
	private CommunicationChannel comm;
	
	@OneToOne(mappedBy = "interview")
	@JsonBackReference(value = "hiring_back")
	private HiringProcess hiring;
	
	@OneToMany(mappedBy = "interview", cascade = CascadeType.ALL)
	@JsonManagedReference
	@JsonIgnore
	private List<InterviewParticipant> participants;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "quiz_id")
	@JsonBackReference(value = "defaultReference")
	private Quiz quiz;
	
	@Transient
	private Map<String, String> additionalInformations = new HashMap<String, String>();

	@JsonAnyGetter
	public Map<String, String> getAdditionalInformation() {
		return additionalInformations;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformations = additionalInformation;
	}
	
	@JsonAnySetter 
	public void setAdditionalInformation(final String key, final String value) { 
		this.additionalInformations.put(key, value); 
	}
}
