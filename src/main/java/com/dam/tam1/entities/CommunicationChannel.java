package com.dam.tam1.entities;

public enum CommunicationChannel {
    ON_SITE, ONLINE;
}
