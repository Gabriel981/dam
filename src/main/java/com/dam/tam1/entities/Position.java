package com.dam.tam1.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Position implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long position_id;
	
	@NotNull
	@NotEmpty(message = "The position name cannot be empty")
	@Min(value = 4, message = "The position name cannot be less than 4 characters")
	@Max(value = 20, message = "The position name cannot be more than 20 characters")
	private String positionName;
	
	@NotNull
	@NotEmpty(message = "The description cannot be empty")
	@Min(value = 15, message = "The short description cannot be less than 15 characters")
	@Max(value = 200, message = "The short description cannot be more than 200 characters")
	private String shortDescription;
	
	private Boolean isAvailable;
	
	private Integer no_available_positions;
	
	@OneToMany(mappedBy = "position", cascade = CascadeType.ALL)
	@JsonManagedReference(value = "position_back")
	private List<Quiz> quizzes;
	
	@OneToMany(mappedBy = "position", cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<Question> questions;
}
