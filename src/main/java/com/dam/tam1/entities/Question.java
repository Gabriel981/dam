package com.dam.tam1.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "questions")
public class Question implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long question_id;
	
	@NotNull
	@NotEmpty(message = "The question body cannot be empty")
	@Min(value = 8, message = "The question body cannot be less than 8 characters")
	@Max(value = 100, message = "The question body cannot be more than 100 characters")
	private String question_body;
	
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "position_id")
    @JsonBackReference
	private Position position;
	
    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL)
    @JsonManagedReference
	private List<Answer> answers;
	
    @OneToMany(mappedBy = "question")
	private List<QuestionQuiz> quizes;
	
	private Integer correctAnswer;

	public Question(Long question_id, @NotEmpty(message = "The question body cannot be empty") String question_body,
			Position position) {
		super();
		this.question_id = question_id;
		this.question_body = question_body;
		this.position = position;
	}
	
	
}
