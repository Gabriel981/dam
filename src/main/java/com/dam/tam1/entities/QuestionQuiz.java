package com.dam.tam1.entities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class QuestionQuiz implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private QuestionQuizPk questionQuizPk;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "question_id", insertable = false, updatable = false)
	private Question question;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quiz_id", insertable = false, updatable = false)
	private Quiz quiz;

	public QuestionQuiz(QuestionQuizPk questionQuizPk) {
		this.questionQuizPk = questionQuizPk;
	}
}
