package com.dam.tam1.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class QuestionQuizPk implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name="question_id")
	private Long questionId;
	
	@Column(name="quiz_id")
	private Long quizId;

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public Long getQuizId() {
		return quizId;
	}

	public void setQuizId(Long quizId) {
		this.quizId = quizId;
	}

	public QuestionQuizPk() {
	}
	
	
	
}
