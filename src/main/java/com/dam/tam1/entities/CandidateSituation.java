package com.dam.tam1.entities;

public enum CandidateSituation {
	Waiting,
	    UnderReview,
	        ReadyForInterview,
	            ScheduledInterview,
	                 ScheduledEvaluation,
	                    Accepted,
	                        Rejected,
	                            WaitingForFinalResponse;
}
