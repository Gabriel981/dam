package com.dam.tam1.entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "hiring_processes")
public class HiringProcess implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long hiring_id;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "interview_id")
	@JsonManagedReference(value = "hiring_back")
	private Interview interview;

	@OneToOne(mappedBy = "hiring", cascade = CascadeType.PERSIST)
	@JsonBackReference
	private Submission submission;

	@Enumerated(EnumType.STRING)
	private CandidateSituation candidateSituation;

	private String mentions;

	@Transient
	private Map<String, Object> additionalInformations = new HashMap<String, Object>();

	@JsonAnyGetter
	public Map<String, Object> getAdditionalInformation() {
		return additionalInformations;
	}

	public void setAdditionalInformation(Map<String, Object> additionalInformation) {
		this.additionalInformations = additionalInformation;
	}
	
	@JsonAnySetter 
	public void setAdditionalInformation(final String key, final Object value) { 
		this.additionalInformations.put(key, value); 
	}

}
