package com.dam.tam1.data;

import com.dam.tam1.entities.Question;
import com.dam.tam1.entities.QuestionQuizPk;
import com.dam.tam1.entities.Quiz;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuestionQuizDTO {

	private QuestionQuizPk questionQuizPk;
	
	private Question question;
	
	private Quiz quiz;

}
