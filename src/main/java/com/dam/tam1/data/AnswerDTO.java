package com.dam.tam1.data;

import com.dam.tam1.entities.Question;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AnswerDTO {

	private Long answer_id;
	
	private Question question;
	
	private String answerBody;
}
