package com.dam.tam1.data;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import com.dam.tam1.entities.Candidate;
import com.dam.tam1.entities.CommunicationChannel;
import com.dam.tam1.entities.Position;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonDeserialize
public class InterviewReadDTO {

	private Long interview_id;
	
	private Date interviewDate;
	
	private CommunicationChannel comm;
	
	private Candidate candidate;
	
	private Position position;
	
	@Transient
	private Map<String, String> additionalInformations = new HashMap<String, String>();

	@JsonAnyGetter
	public Map<String, String> getAdditionalInformation() {
		return additionalInformations;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformations = additionalInformation;
	}
	
	@JsonAnySetter 
	public void setAdditionalInformation(final String key, final String value) { 
		this.additionalInformations.put(key, value); 
	}
}
