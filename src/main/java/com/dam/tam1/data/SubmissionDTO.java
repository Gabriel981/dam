package com.dam.tam1.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.dam.tam1.entities.Candidate;
import com.dam.tam1.entities.HiringProcess;
import com.dam.tam1.entities.Position;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SubmissionDTO {
	
	private Long id;
	
	private Date dateOfSubmission;
	
	@Min(value= 10, message = "The mentionat cannot be less than 5 characters")
	@Max(value = 150, message = "The mention cannot be more than 150 characters")
	private String mention;
	
	private Position appliedPosition;
	
	private Candidate candidate;
	
	private HiringProcess hiring;
	
	private Map<String, Object> additionalInformations = new HashMap<String, Object>();

	@JsonAnyGetter
	public Map<String, Object> getAdditionalInformation() {
		return additionalInformations;
	}

	public void setAdditionalInformation(Map<String, Object> additionalInformation) {
		this.additionalInformations = additionalInformation;
	}
	
	@JsonAnySetter 
	public void setAdditionalInformation(final String key, final String value) { 
		this.additionalInformations.put(key, value); 
	}
}
