package com.dam.tam1.data;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.dam.tam1.entities.Candidate;
import com.dam.tam1.entities.Position;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SubmissionReadDTO {

	private Long id;
	
	private Date dateOfSubmission;
	
	private String mention;
	
	private Position appliedPosition;
	
	private Candidate candidate;
	
	private Map<String, Object> additionalInformations = new HashMap<String, Object>();

	@JsonAnyGetter
	public Map<String, Object> getAdditionalInformation() {
		return additionalInformations;
	}

	public void setAdditionalInformation(Map<String, Object> additionalInformation) {
		this.additionalInformations = additionalInformation;
	}
	
	@JsonAnySetter 
	public void setAdditionalInformation(final String key, final String value) { 
		this.additionalInformations.put(key, value); 
	}
	
}
