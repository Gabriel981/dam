package com.dam.tam1.data;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.dam.tam1.entities.Interview;
import com.dam.tam1.entities.InterviewParticipantPk;
import com.dam.tam1.entities.Participant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InterviewParticipantDTO {
	
	private InterviewParticipantPk interviewParticipantPk;
	
	private Interview interview;
	
	private Participant participant;
	
	private Date dateOfAllocation;
	
	private Map<String, Object> additionalInformations = new HashMap<String, Object>();

}
