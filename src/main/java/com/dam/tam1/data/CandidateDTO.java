package com.dam.tam1.data;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CandidateDTO {
	
	private Long candidate_id;	
	@NotEmpty(message = "The first name cannot be empty")
	private String firstName;
	@NotEmpty(message = "The last name cannot be empty")
	private String lastName;
	@NotEmpty(message = "The address cannot be empty")
	private String address;
	
	private Date dob;
	
	@Email
	@NotEmpty(message = "The email address cannot be empty")
	private String emailAddress;
	
	@NotEmpty(message = "The phone no. cannot be empty")
	private String phoneNo;

}
