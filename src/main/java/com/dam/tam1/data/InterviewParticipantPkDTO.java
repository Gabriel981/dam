package com.dam.tam1.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InterviewParticipantPkDTO {

	private Long interviewId;
	
	private Long participantId;
}
