package com.dam.tam1.data;

import java.util.HashMap;
import java.util.Map;

import com.dam.tam1.entities.CandidateSituation;
import com.dam.tam1.entities.Interview;
import com.dam.tam1.entities.Submission;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HiringDTO {

	private Long hiring_id;

	private Interview interview;

	private Submission submission;

	private CandidateSituation candidateSituation;

	private String mentions;

	private Map<String, Object> additionalInformations = new HashMap<String, Object>();
}
