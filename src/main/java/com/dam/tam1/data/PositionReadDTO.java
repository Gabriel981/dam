package com.dam.tam1.data;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PositionReadDTO {

	private Long position_id;
	
	@NotEmpty(message = "The position name cannot be empty")
	private String positionName;
	
	private String shortDescription;
	
	private Boolean isAvailable;
	
	private Integer no_available_positions;
}
