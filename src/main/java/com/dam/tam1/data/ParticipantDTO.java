package com.dam.tam1.data;

import java.util.Date;

import com.dam.tam1.entities.Position;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ParticipantDTO {
	
	private Long employee_id;
	
	private String firstName;

	private String lastName;

	private String address;
	
	private String emailAddress;
	
	private Date dateOfBirth;
    
	private String phoneNo;
	
	private Position position;

	private String participant_role;
	
	private String mentions;
}
