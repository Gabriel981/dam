package com.dam.tam1.data;

import com.dam.tam1.entities.Position;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class QuizDTO {

	private Long quiz_id;
	
	private String mentions;
	
	private Integer score_result;

	private Position position;
	
}
