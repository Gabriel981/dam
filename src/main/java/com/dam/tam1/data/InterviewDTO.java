package com.dam.tam1.data;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.dam.tam1.entities.CommunicationChannel;
import com.dam.tam1.entities.HiringProcess;
import com.dam.tam1.entities.Quiz;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InterviewDTO {
	
	private Long interview_id;
	
	private Date interviewDate;
	
	private CommunicationChannel comm;
	
	private HiringProcess hiring;
	
	private Quiz quiz;
	
	private Map<String, String> additionalInformations = new HashMap<String, String>();
}
