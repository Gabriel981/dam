package com.dam.tam1.data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PositionDTO {
	
	private Long position_id;
	
	@NotEmpty(message = "The position name cannot be empty")
	private String positionName;
	
	@Min(value = 5, message = "The short description cannot be less than 5 characters")
	@Max(value = 150, message = "The description cannot be more than 150 characters")
	private String shortDescription;
	
	private Boolean isAvailable;
	
	private Integer no_available_positions;
	
}
