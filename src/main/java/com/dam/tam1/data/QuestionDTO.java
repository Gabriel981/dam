package com.dam.tam1.data;

import com.dam.tam1.entities.Position;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class QuestionDTO {
	
	private Long question_id;
	
	private String question_body;
	
    private Position position;
	
	private Integer correctAnswer;
	
}
