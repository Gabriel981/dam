package com.dam.tam1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.dam.tam1.services.CandidateService;

@SpringBootApplication
public class Tam1Application {
	
	@Autowired
	private static CandidateService candidateService;
	
	public static void main(String[] args) {
		SpringApplication.run(Tam1Application.class, args);	
	}
}
