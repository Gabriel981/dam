package com.dam.tam1.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dam.tam1.data.SubmissionDTO;
import com.dam.tam1.data.SubmissionReadDTO;
import com.dam.tam1.entities.Submission;
import com.dam.tam1.services.IHiringFlowService;
import com.dam.tam1.services.SubmissionService;
import com.sun.el.parser.ParseException;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SubmissionController {

	@Autowired
	private SubmissionService submissionService;
	
	@Autowired
	private IHiringFlowService iHiringFlowService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping("/submissions")
	public List<SubmissionReadDTO> getSubmissions() {
		List<Submission> submissions = this.submissionService.getAllSubmissions();
		
		return submissions.stream().map(this::convertToDto)
				.collect(Collectors.toList());
	}
	
	@PostMapping(value = "/submissions/add", produces = "application/json")
	public void addSubmission(@RequestBody Submission submission) {
		this.iHiringFlowService.entrySubmissionWithCandidate(submission);
	}
	
	@GetMapping(value = "/submissions/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public SubmissionReadDTO getSubmissionById(@PathVariable Integer id) {
		Submission submissionFound = this.submissionService.getSubmissionById(id);
		return convertToDto(submissionFound);
	}
	
	@PutMapping(value="/submissions/update/id/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void updateSubmission(@PathVariable Integer id, @RequestBody SubmissionDTO submissionDTO) throws ParseException {
		Submission submission = convertToEntity(submissionDTO);
		this.submissionService.updateSubmission(submission);
	}

	@DeleteMapping(value = "/submissions/delete/{id}")
	public void deleteSubmission(@PathVariable(value = "id") Integer id) {
//		this.iHiringFlowService.withdrawSubmission(id);
		this.submissionService.deleteSubmissionById(id);
	}
	
	//Convert to DTO classes (just for Submission model)
	private SubmissionReadDTO convertToDto(Submission submission) {
		SubmissionReadDTO submissionDTO = modelMapper.map(submission, SubmissionReadDTO.class);
		return submissionDTO;
	}
	
	//Convert to Entity classes (just for Submission model)
	private Submission convertToEntity(SubmissionDTO submissionDTO) throws ParseException{
		Submission submission = modelMapper.map(submissionDTO, Submission.class);
		return submission;
	}
}
