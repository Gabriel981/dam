package com.dam.tam1.controllers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dam.tam1.data.AnswerDTO;
import com.dam.tam1.data.QuestionDTO;
import com.dam.tam1.entities.Answer;
import com.dam.tam1.entities.Question;
import com.dam.tam1.services.QuestionService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class QuestionController {

	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping(value = "/questions")
	public Collection<QuestionDTO> getAllQuestions(){
		return this.questionService.getAllQuestions().stream().map(this::convertToDTO).collect(Collectors.toList());
	}
	
	@GetMapping(value = "/question_answers/{id}")
	public Collection<AnswerDTO> getAllAnswers(@PathVariable Integer id){
		return this.questionService.getAllAnswersForQuestion(id).stream().map(this::convertToDTO).collect(Collectors.toList());
	}
	
	@GetMapping(value = "/questions/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public QuestionDTO getQuestionById(@PathVariable Integer id) {
		return convertToDTO(this.questionService.getQuestionById(id));
	}
	
	@PostMapping(value = "/questions/add/")
	@ResponseStatus(HttpStatus.OK)
	public QuestionDTO addQuestion(@RequestBody QuestionDTO questionDTO) {
		return convertToDTO(this.questionService.saveQuestion(convertToEntity(questionDTO)));
	}
	
	@PutMapping(value = "/questions/update/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void updateQuestion(@PathVariable(value="id") Integer id, @RequestBody QuestionDTO questionDTO) {
		this.questionService.updateQuestion(convertToEntity(questionDTO));
	}
	
	@DeleteMapping(value = "/questions/delete/{id}")
	public void deleteQuestion(@PathVariable(value = "id") Integer id) {
		this.questionService.deleteQuestion(id);
	}
	
	private QuestionDTO convertToDTO(Question question) {
		QuestionDTO questionDTO = modelMapper.map(question, QuestionDTO.class);
		return questionDTO;
	}
	
	private Question convertToEntity(QuestionDTO questionDTO) {
		Question question = modelMapper.map(questionDTO, Question.class);
		return question;
	}
	
	private AnswerDTO convertToDTO(Answer answer) {
		AnswerDTO answerDTO = modelMapper.map(answer, AnswerDTO.class);
		return answerDTO;
	}
	
}
