package com.dam.tam1.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dam.tam1.data.HiringDTO;
import com.dam.tam1.entities.HiringProcess;
import com.dam.tam1.services.HiringService;

@RestController
@CrossOrigin(origins = {"http://localhost:4200"})
public class HiringController {
	
	@Autowired
	private HiringService hiringService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping(value="/hiring_processes")
	public List<HiringDTO> getAllHiringProcesses(){
		return this.hiringService.getAllHiringProcesses().stream().map(this::convertToDTO).collect(Collectors.toList());
	}
	
	@GetMapping(value = "/hiring_processes/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public HiringDTO getHiringProcessById(@PathVariable Integer id) {
		return this.convertToDTO(this.hiringService.getHiringProcessById(id));
	}
	
	@PatchMapping("/hiring_processes/update/{id}")
	public void updateHiringProcess(@PathVariable("id") Integer id, @RequestBody HiringProcess hiringProcess) {
		this.hiringService.updateHiringProcess(hiringProcess);
	}
	
	@DeleteMapping(value="hiring_processes/delete/{id}")
	public void deleteHiringProcces(@PathVariable("id") Integer id, @RequestBody HiringProcess hiringProcess) {
		this.hiringService.deleteHiringProcess(hiringProcess);
	}
	
	public HiringDTO convertToDTO(HiringProcess hiringProcess) {
		HiringDTO hiringDTO = this.modelMapper.map(hiringProcess, HiringDTO.class);
		return hiringDTO;
	}
	
	public HiringProcess convertToEntity(HiringDTO hiringDTO) {
		HiringProcess hiringProcess = this.modelMapper.map(hiringDTO, HiringProcess.class);
		return hiringProcess;
	}
}
