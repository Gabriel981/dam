package com.dam.tam1.controllers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dam.tam1.data.InterviewDTO;
import com.dam.tam1.data.InterviewParticipantDTO;
import com.dam.tam1.data.InterviewParticipantPkDTO;
import com.dam.tam1.data.ParticipantDTO;
import com.dam.tam1.entities.Interview;
import com.dam.tam1.entities.InterviewParticipant;
import com.dam.tam1.entities.InterviewParticipantPk;
import com.dam.tam1.entities.Participant;
import com.dam.tam1.services.InterviewParticipantService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class InterviewParticipantController {

	@Autowired
	private InterviewParticipantService service;

	@Autowired
	private ModelMapper modelMapper;

	@GetMapping("/interviews_participants")
	public Collection<InterviewParticipantDTO> getInterviewParticipants(){
		return this.service.getAllParticipantsOnInterview().stream().map(this::convertToDto).collect(Collectors.toList());
	}
	
	@GetMapping("/interviews_participants_1/{id}")
	public Collection<InterviewDTO> getInterviewsByParticipantId(@PathVariable(value = "id") Integer id){
		return this.service.getInterviewsByParticipantId(id).stream().map(this::convertToDTO).collect(Collectors.toList());
	}
	
	@GetMapping("/interviews_participants_2/{id}")
	public Collection<ParticipantDTO> getParticipantsByInterviewId(@PathVariable(value = "id") Integer id){
		return this.service.getParticipantsByInterviewId(id).stream().map(this::convertToDTO).collect(Collectors.toList());
	}
	
	@PostMapping(value = "/interviews_participants/add", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public InterviewParticipantDTO scheduleInterviewParticipant(@RequestBody InterviewParticipantDTO interviewParticipant) {
		return convertToDto(this.service.saveInterviewParticipant(convertToEntity(interviewParticipant)));
	}
	
	@PostMapping(value = "/interviews_participants/delete/")
	public void deleteInterviewParticipant(@RequestBody InterviewParticipantPkDTO interviewParticipantPkDTO) {
		InterviewParticipantPk interviewParticipantPk = new InterviewParticipantPk();
		interviewParticipantPk.setInterviewId(interviewParticipantPkDTO.getInterviewId());
		interviewParticipantPk.setParticipantId(interviewParticipantPkDTO.getParticipantId());
		this.service.deleteInterviewParticipantById(interviewParticipantPk);
	}
	
	private InterviewParticipantDTO convertToDto(InterviewParticipant interviewParticipant) {
		InterviewParticipantDTO interviewParticipantDTO = modelMapper.map(interviewParticipant, InterviewParticipantDTO.class);
		return interviewParticipantDTO;
	}
	
	private InterviewParticipant convertToEntity(InterviewParticipantDTO interviewParticipantDTO) {
		InterviewParticipant interviewParticipant = modelMapper.map(interviewParticipantDTO, InterviewParticipant.class);
		return interviewParticipant;
	}
	
	private InterviewDTO convertToDTO(Interview interview) {
		InterviewDTO interviewDTO = modelMapper.map(interview, InterviewDTO.class);
		return interviewDTO;
	}
	
	private ParticipantDTO convertToDTO(Participant participant) {
		ParticipantDTO participantDTO = modelMapper.map(participant, ParticipantDTO.class);
		return participantDTO;
	}
	
	private InterviewParticipantPkDTO convertToDTO(InterviewParticipantPk interviewParticipantPk) {
		InterviewParticipantPkDTO interviewParticipantPkDTO = modelMapper.map(interviewParticipantPk, InterviewParticipantPkDTO.class);
		return interviewParticipantPkDTO;
	}
	
	private InterviewParticipantPk convertToEntity(InterviewParticipantPkDTO interviewParticipantPkDTO) {
		InterviewParticipantPk interviewParticipantPk = modelMapper.map(interviewParticipantPkDTO, InterviewParticipantPk.class);
		return interviewParticipantPk;
	}
	
}
