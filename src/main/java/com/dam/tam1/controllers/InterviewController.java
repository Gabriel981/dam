package com.dam.tam1.controllers;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dam.tam1.data.InterviewDTO;
import com.dam.tam1.data.InterviewReadDTO;
import com.dam.tam1.data.ParticipantDTO;
import com.dam.tam1.entities.Interview;
import com.dam.tam1.entities.Participant;
import com.dam.tam1.services.IHiringFlowService;
import com.dam.tam1.services.InterviewService;
import com.sun.el.parser.ParseException;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class InterviewController {

	@Autowired
	private IHiringFlowService hiringFlowService;
	
	@Autowired
	private InterviewService interviewService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@PostMapping(value="/interviews/schedule/")
	@ResponseStatus(HttpStatus.CREATED)
	private InterviewDTO scheduleInterview(@RequestBody InterviewDTO interviewDTO) throws ParseException {
		return convertToDto(this.hiringFlowService.scheduleSimpleInterview(convertToEntity(interviewDTO)));
	}
	
	@GetMapping(value="/interviews")
	private List<InterviewDTO> getAllInterviews(){
		return this.interviewService.getAllInterviews().stream()
				.map(this::convertToDto)
				.collect(Collectors.toList());
	}
	
	@GetMapping(value = "/interviews/participants/{id}")
	private Collection<ParticipantDTO> getAllParticipantsByInterview(@PathVariable Integer id){
		return this.interviewService.getAllParticipantsByInterviewId(id).stream().map(this::convertToDTOParticipant).collect(Collectors.toList());
	}
	
	@GetMapping(value="/interviews/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	private InterviewDTO getInterviewById(@PathVariable Integer id) {
		return convertToDto(this.interviewService.getInterviewById(id));
	}
	
	@PutMapping(value="/interviews/update/{id}")
	@ResponseStatus(HttpStatus.OK)
	private void updateInterview(@PathVariable Integer id, @RequestBody InterviewDTO interviewDTO) throws ParseException {
		this.interviewService.updateInterview(convertToEntity(interviewDTO));
	}
	
	@DeleteMapping(value="/interviews/delete/{id}")
	private void deleteInterview(@PathVariable Integer id) {
		this.interviewService.deleteInterviewById(id);
	}
	
	//Convert to DTO classes (just for Interview model)
	private InterviewDTO convertToDto(Interview interview) {
		InterviewDTO interviewDTO = modelMapper.map(interview, InterviewDTO.class);
		return interviewDTO;
	}
	
	//Convert to Entity classes (just for Interview model)
	private Interview convertToEntity(InterviewDTO interviewDTO) throws ParseException{
		Interview interview = modelMapper.map(interviewDTO, Interview.class);
		return interview;
	}
	
	private ParticipantDTO convertToDTOParticipant(Participant participant) {
		ParticipantDTO participantDTO = this.modelMapper.map(participant, ParticipantDTO.class);
		return participantDTO;
	}
	
	
	
	
}

