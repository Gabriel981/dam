package com.dam.tam1.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dam.tam1.data.PositionDTO;
import com.dam.tam1.data.PositionReadDTO;
import com.dam.tam1.entities.Position;
import com.dam.tam1.services.PositionService;

@RestController
@CrossOrigin
public class PositionController {

	@Autowired
	private PositionService positionService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping("/positions")
	public List<PositionReadDTO> getAllPositions(){
		List<Position> positions = this.positionService.getAllPositions();
		
		return positions.stream().map(this::convertToDto)
			.collect(Collectors.toList());
	}

	@GetMapping(value="/positions/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public PositionReadDTO getPositionById(@PathVariable Integer id) {
		Position positionFound = this.positionService.getPositionById(id);
		
		return convertToDto(positionFound);
	}
	
	@PostMapping(value="/positions/add", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public PositionReadDTO addPosition(@RequestBody PositionDTO positionDTO) {
		Position position = convertToEntity(positionDTO);
		position.setIsAvailable(true);
		Position createdPosition = this.positionService.savePosition(position);
		
		return convertToDto(createdPosition);
	}
	
	@PutMapping("/positions/update/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void updatePosition(@PathVariable Integer id, @RequestBody PositionDTO positionDTO) {
		Position position = convertToEntity(positionDTO);
		this.positionService.updatePosition(position);
	}
	
	@DeleteMapping("/positions/delete/{id}")
	public void deletePosition(@PathVariable Integer id) {
		this.positionService.deletePosition(id);
	}
	
	private PositionReadDTO convertToDto(Position position) {
		PositionReadDTO positionDTO = modelMapper.map(position, PositionReadDTO.class);
		return positionDTO;
	}
	
	private Position convertToEntity(PositionDTO positionDTO) {
		Position posititon = modelMapper.map(positionDTO, Position.class);
		return posititon;
	}
	
}
