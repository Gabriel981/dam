package com.dam.tam1.controllers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dam.tam1.data.AnswerDTO;
import com.dam.tam1.entities.Answer;
import com.dam.tam1.services.AnswerService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AnswerController {

	@Autowired
	private AnswerService answerService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping(value = "/answers/")
	public Collection<AnswerDTO> getAllAnswers(){
		return this.answerService.getAllAnswers().stream().map(this::convertToDTO).collect(Collectors.toList());
	}
	
	@PostMapping(value = "/answers/add/")
	@ResponseStatus(HttpStatus.OK)
	public AnswerDTO addAnswer(@RequestBody AnswerDTO answerDTO) {
		return convertToDTO(this.answerService.saveAnswer(convertToEntity(answerDTO)));
	}
	
	@PutMapping(value = "/answers/update/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void updateAnswer(@PathVariable Integer id, @RequestBody AnswerDTO answerDTO) {
		this.answerService.updateAnswer(convertToEntity(answerDTO));
	}
	
	@DeleteMapping(value = "/answers/delete/{id}")
	public void deleteAnswer(@PathVariable Integer id) {
		this.answerService.deleteAnswerById(id);
	}
	
	private AnswerDTO convertToDTO(Answer answer) {
		AnswerDTO answerDTO = modelMapper.map(answer, AnswerDTO.class);
		return answerDTO;
	}
	
	public Answer convertToEntity(AnswerDTO answerDTO) {
		Answer answer = modelMapper.map(answerDTO, Answer.class);
		return answer;
	}
}
