package com.dam.tam1.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dam.tam1.data.CandidateDTO;
import com.dam.tam1.entities.Candidate;
import com.dam.tam1.services.CandidateService;
import com.sun.el.parser.ParseException;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CandidateController {

	@Autowired
	private CandidateService candidateService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	// Version 1 (With DTO)
	
	@GetMapping(value="/candidates")
	public List<CandidateDTO> getCandidates(){
		List<Candidate> candidates = this.candidateService.getAllCandidates();
		
		return candidates.stream()
				.map(this::convertToDto)
				.collect(Collectors.toList());
	}
	
	@GetMapping(value="/candidates/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public CandidateDTO getCandidateById(@PathVariable Integer id) {
		Candidate candidateFound = this.candidateService.getCandidateById(id);	
		return convertToDto(candidateFound);
	}
	
	@PostMapping(value= "candidates/add")
	@ResponseStatus(HttpStatus.CREATED)
	public CandidateDTO addCandidate(@RequestBody CandidateDTO candidateDto) throws ParseException {
		Candidate candidate = convertToEntity(candidateDto);
		Candidate createdCandidate = this.candidateService.saveCandidate(candidate);
		
		return convertToDto(createdCandidate);
	}
	
	@PutMapping(value="/candidates/update/id/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void updateCandidate(@PathVariable(value = "id") Integer id, @RequestBody CandidateDTO candidateDto) throws ParseException {
		Candidate candidate = convertToEntity(candidateDto);
		this.candidateService.updateCandidate(candidate);
	}
	
	@DeleteMapping(value="/candidates/delete/{id}")
	public void deleteCandidate(@PathVariable Integer id) {
		this.candidateService.deleteCandidate(id);
	}

	
	//Convert to DTO classes (just for Candidate model)
	private CandidateDTO convertToDto(Candidate candidate) {
		CandidateDTO candidateDTO = modelMapper.map(candidate, CandidateDTO.class);
		return candidateDTO;
	}
	
	//Convert to Entity classes (just for Candidate model)
	private Candidate convertToEntity(CandidateDTO candidateDTO) throws ParseException{
		Candidate candidate = modelMapper.map(candidateDTO, Candidate.class);
		return candidate;
	}
	
	// Version 2 (withoud DTO)
	
//		@GetMapping(value = "/candidates")
//		public List<Candidate> getCandidates(){
//			return this.candidateService.getAllCandidates();
//		}
	//	
//		@PostMapping(value= "candidates/add")
//		public Candidate addCandidate(@RequestBody Candidate candidate) {
//			return this.candidateService.saveCandidate(candidate);
//		}
	//	
//		@GetMapping(value = "/candidates/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
//		public Candidate getCandidateById(@PathVariable Integer id){
//			return this.candidateService.getCandidateById(id);
//		}
	//	
//		@PutMapping(value="/candidates/update/id/{id}")
//		public Candidate updateCandidate(@PathVariable(value = "id") Integer id, @RequestBody Candidate candidate) {
//			return this.candidateService.updateCandidate(candidate);
//		}
	//	
//		@DeleteMapping(value="/candidates/delete/{id}")
//		public void deleteCandidate(@PathVariable Integer id) {
//			this.candidateService.deleteCandidate(id);
//		}
	
}
