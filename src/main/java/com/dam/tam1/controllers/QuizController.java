package com.dam.tam1.controllers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dam.tam1.data.QuestionDTO;
import com.dam.tam1.data.QuizDTO;
import com.dam.tam1.entities.Quiz;
import com.dam.tam1.services.QuizService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class QuizController {

	@Autowired
	private QuizService quizService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping(value = "/quizes/")
	public Collection<QuizDTO> getAllQuizes(){
		return this.quizService.getAllQuizes().stream().map(this::convertToDTO).collect(Collectors.toList());
	}
	
	@GetMapping(value = "/quizes/{id}")
	public QuizDTO getQuizById(@PathVariable Integer id) {
		return convertToDTO(this.quizService.getQuizById(id));
	}
	
	@PostMapping(value = "/quizes/add")
	@ResponseStatus(HttpStatus.OK)
	public QuizDTO addQuiz(@RequestBody QuizDTO quizDTO) {
		return convertToDTO(this.quizService.saveQuiz(convertToEntity(quizDTO)));
	}
	
	@PutMapping(value = "/quizes/update/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void updateQuiz(@PathVariable Integer id, @RequestBody QuizDTO quizDTO) {
		this.quizService.saveQuiz(convertToEntity(quizDTO));
	}
	
	@DeleteMapping(value = "/quizes/delete/{id}")
	public void deleteQuiz(@PathVariable Integer id) {
		this.quizService.deleteQuizById(id);
	}
	
	public QuizDTO convertToDTO(Quiz quiz) {
		QuizDTO quizDTO = modelMapper.map(quiz, QuizDTO.class);
		return quizDTO;
	}
	
	public Quiz convertToEntity(QuizDTO quizDTO) {
		Quiz quiz = modelMapper.map(quizDTO, Quiz.class);
		return quiz;
	}
	
}
