package com.dam.tam1.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dam.tam1.data.ParticipantDTO;
import com.dam.tam1.entities.Participant;
import com.dam.tam1.services.ParticipantService;

@RestController
@CrossOrigin
public class ParticipantController {

	@Autowired
	private ParticipantService participantService;
	
	@Autowired
	private ModelMapper modelMapper;

	@GetMapping(value="/participants")
	public List<ParticipantDTO> getAllParticipants(){
		
		return participantService.getAllParticipants()
				.stream()
				.map(this::convertToDto)
				.collect(Collectors.toList());
	}
	
	@GetMapping(value="/participants/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ParticipantDTO getParticipantById(@PathVariable Integer id) {
		return convertToDto(this.participantService.getParticipantById(id));
	}
	
	@PostMapping(value="/participants/add/", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ParticipantDTO addParticipant(@RequestBody ParticipantDTO participantDTO) {
		Participant participant = convertToEntity(participantDTO);
		Participant savedParticipant = this.participantService.saveParticipant(participant);
		
		return this.convertToDto(savedParticipant);
	}
	
	@PutMapping(value="/participants/update/{id}")
	public void updateParticipant(@PathVariable Integer id, @RequestBody ParticipantDTO participantDTO) {
		Participant participant = convertToEntity(participantDTO);
		this.participantService.updateParticipant(participant);
	}
	
	@DeleteMapping(value="/participants/delete/{id}")
	public void deleteParticipant(@PathVariable Integer id) {
		this.participantService.deleteParticipantById(id);
	}
	
	private ParticipantDTO convertToDto(Participant participant) {
		ParticipantDTO participantDTO = modelMapper.map(participant, ParticipantDTO.class);
		return participantDTO;
	}
	
	private Participant convertToEntity(ParticipantDTO participantDTO) {
		Participant participant = modelMapper.map(participantDTO, Participant.class);
		return participant;
	}

	
}
