package com.dam.tam1.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dam.tam1.entities.Answer;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {
    @Override
	List<Answer> findAll();
}
