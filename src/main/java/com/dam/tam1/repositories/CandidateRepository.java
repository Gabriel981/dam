package com.dam.tam1.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dam.tam1.entities.Candidate;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long> {

}
