package com.dam.tam1.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dam.tam1.entities.QuestionQuiz;
import com.dam.tam1.entities.QuestionQuizPk;

@Repository
public interface QuestionQuizRepository extends JpaRepository<QuestionQuiz, QuestionQuizPk> {

}
