package com.dam.tam1.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dam.tam1.entities.InterviewParticipant;
import com.dam.tam1.entities.InterviewParticipantPk;

@Repository
public interface InterviewParticipantRepository extends JpaRepository<InterviewParticipant, InterviewParticipantPk> {

}