package com.dam.tam1.services;

import com.dam.tam1.entities.HiringProcess;

public interface NotificationService {

	public void notificationOnInterview(HiringProcess hiringProcess);
	
	public void notificationOnStatusChange(HiringProcess hiringProcess);

}
