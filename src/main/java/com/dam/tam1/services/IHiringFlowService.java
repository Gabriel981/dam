package com.dam.tam1.services;

import java.util.Date;
import java.util.List;

import com.dam.tam1.entities.CommunicationChannel;
import com.dam.tam1.entities.HiringProcess;
import com.dam.tam1.entities.Interview;
import com.dam.tam1.entities.InterviewParticipant;
import com.dam.tam1.entities.Submission;

public interface IHiringFlowService {

	// used for building a new simple submission for a candidate, including the candidate itself
		// and the position for which he is applying.
		public void entrySubmissionWithCandidate(Submission submission);
		
		public HiringProcess entrySubmissionWithCandidate(Integer candidateId, Integer positionId);

		// schedule an interview for a candidate and a start date that has a default implementation of 2 weeks from now
//	    public Interview scheduleSimpleInterview(HiringProcess hiringProcess, Date startDate);
	    public Interview scheduleSimpleInterview(Interview interview);
		
	    // schedule an interview for a candidate, by including a start date with a default implementation of 2 weeks from now
	    // and a position specified
	    public Interview scheduleInterviewWithDetails(HiringProcess hiringProcess, Date startDate, CommunicationChannel comm);
	    
	    public List<Submission> getSubmissions();
	    
	    public void withdrawSubmission(Integer id);
	    
	    public void allocateParticipantToInterview(InterviewParticipant interviewParticipant);
	    
	    public void changeCandidateStatus(HiringProcess hiringProcess);
	    
	    
}
