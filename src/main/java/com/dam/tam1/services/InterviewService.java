package com.dam.tam1.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.print.attribute.standard.DateTimeAtCompleted;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.dam.tam1.entities.HiringProcess;
import com.dam.tam1.entities.Interview;
import com.dam.tam1.entities.InterviewParticipant;
import com.dam.tam1.entities.Participant;
import com.dam.tam1.entities.Position;
import com.dam.tam1.repositories.InterviewParticipantRepository;
import com.dam.tam1.repositories.InterviewRepository;
import com.dam.tam1.utils.DateUtils4J8API;

@Service
public class InterviewService {

	private InterviewRepository interviewRepository;
	
	private InterviewParticipantRepository interviewParticipantRepository;
	
	private HiringService hiringService;
	
	private Integer nextID = 0;
	
	public Long getNextID() {
		Long nextIDL = Long.valueOf(nextID);
		nextIDL++;
		return nextIDL;
	}

	@Autowired
	public void setInterviewRepository(InterviewRepository interviewRepository) {
		this.interviewRepository = interviewRepository;
	}
	
	@Autowired
	public void setInterviewParticipantRepository(InterviewParticipantRepository interviewParticipantRepository) {
		this.interviewParticipantRepository = interviewParticipantRepository;
	}

	@Autowired
	public void setHiringService(HiringService hiringService) {
		this.hiringService = hiringService;
	}

	public Interview saveInterview(Interview interview) {
		return this.interviewRepository.save(interview);
	}
	
	public void updateInterview(Interview interview) {
		if(interview.getInterviewDate() == null) {
			LocalDateTime interviewDate = LocalDateTime.now().plusWeeks(2);
			interview.setInterviewDate(DateUtils4J8API.asDate(interviewDate));
			this.saveInterview(interview);
		} else {
			this.saveInterview(interview);
		}
	}
	
	public void deleteInterview(Interview interview) {
		this.interviewRepository.delete(interview);
	}
	
	public void deleteInterviewById(Integer id) {
		Long long_id = Long.valueOf(id);
		HiringProcess hiringFound = this.hiringService.getHiringProcessById(this.interviewRepository.findById(long_id).get().getHiring().getHiring_id().intValue());
		hiringFound.setInterview(null);
		this.hiringService.updateHiringProcess(hiringFound);
		this.interviewRepository.deleteById(long_id);
	}
	
	public Interview getInterviewById(Integer id) {
		Long long_id = Long.valueOf(id);
		Optional<Interview> interviewFound = this.interviewRepository.findById(long_id);
		Interview interview = interviewFound.get();
		
		return interview;
	}
	
	public List<Interview> getInterviewsByPosition(Position position) {
		List<Interview> persistenceInterviews = this.interviewRepository.findAll();
		List<Interview> interviewsByPosition = new ArrayList<Interview>();
		
		for(Interview i : persistenceInterviews) {
			if(i.getHiring().getSubmission().getAppliedPosition().equals(position)) {
				interviewsByPosition.add(i);
			}
		}
		
		return interviewsByPosition;
	}
	
	public List<Interview> getAllInterviews(){
		List<Interview> interviews = this.interviewRepository.findAll();
		
		for(Interview in : interviews) {
			in.getAdditionalInformations().put("firstName", in.getHiring().getSubmission().getCandidate().getFirstName());
			in.getAdditionalInformation().put("lastName", in.getHiring().getSubmission().getCandidate().getLastName());
			in.getAdditionalInformation().put("hiring_id", in.getHiring().getHiring_id().toString());
		}
		
		return interviews;
	}
	
	public InterviewParticipant saveParticipantInterview(InterviewParticipant interviewParticipant) {
		return this.interviewParticipantRepository.save(interviewParticipant);
	}
	
	public void updateParticipantInterview(InterviewParticipant interviewParticipant) {
		Optional<InterviewParticipant> interviewFound = this.interviewParticipantRepository.findById(interviewParticipant.getInterviewParticipantPk());
		InterviewParticipant updatedInterview = interviewFound.get();
		
		if(!updatedInterview.equals(null)) {
			updatedInterview.setDateOfAllocation(interviewParticipant.getDateOfAllocation());
			this.interviewParticipantRepository.save(updatedInterview);
		}
	}
	
	public void deleteParticipantInterview(Participant participant, InterviewParticipant interviewParticipant) {
		this.interviewParticipantRepository.delete(interviewParticipant);
	}
	
	public List<InterviewParticipant> getInterviewParticipants(){
		return this.interviewParticipantRepository.findAll();
	}
	
	public Collection<Participant> getAllParticipantsByInterviewId(Integer id){
		Long long_id = Long.valueOf(id);
		Interview interviewFound = this.interviewRepository.findById(long_id).get();
		
		Collection<Participant> participantsCollected = new ArrayList<>();
		
		Collection<InterviewParticipant> interviewsParticipants = this.interviewParticipantRepository.findAll();
		for(InterviewParticipant interview_participant: interviewsParticipants) {
			if(interview_participant.getInterview() == interviewFound) {
				participantsCollected.add(interview_participant.getParticipant());
			}
		}
		
		return participantsCollected;
	}
	
	
}
