package com.dam.tam1.services;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dam.tam1.entities.Interview;
import com.dam.tam1.entities.QuestionQuiz;
import com.dam.tam1.entities.Quiz;
import com.dam.tam1.repositories.QuestionQuizRepository;
import com.dam.tam1.repositories.QuizRepository;

@Service
public class QuizService {

	private QuizRepository quizRepository;
	
	private QuestionQuizRepository questionQuizRepository;
	
	private InterviewService interviewService;
	
	@Autowired
	public void setQuizRepository(QuizRepository quizRepository) {
		this.quizRepository = quizRepository;
	}
	
	@Autowired
	public void setQuestionQuizRepository(QuestionQuizRepository questionQuizRepository) {
		this.questionQuizRepository = questionQuizRepository;
	}

	@Autowired
	public void setInterviewService(InterviewService interviewService) {
		this.interviewService = interviewService;
	}

	public Quiz saveQuiz(Quiz quiz) {
		return this.quizRepository.save(quiz);
	}
	
	public void updateQuiz(Quiz quiz) {
		this.quizRepository.save(quiz);
	}
	
	public void deleteQuizById(Integer id) {
		Long long_id = Long.valueOf(id);
		Quiz quizFound = this.quizRepository.findById(long_id).get();
		Collection<Interview> interviews = this.interviewService.getAllInterviews();
		
		for(Interview interview : interviews) {
			if(interview.getQuiz() == quizFound) {
				interview.setQuiz(null);
				this.interviewService.updateInterview(interview);
			}
		}
		
		this.quizRepository.deleteById(long_id);
	}
	
	public Quiz getQuizById(Integer id) {
		Long quizID = Long.valueOf(id);
		return this.quizRepository.findById(quizID).get();
	}
	
	public List<Quiz> getAllQuizes(){
		return this.quizRepository.findAll();
	}
	
	public QuestionQuiz saveQuestionQuiz(QuestionQuiz questionQuiz) {
		return this.questionQuizRepository.save(questionQuiz);
	}
	
	public void deleteQuestionQuiz(QuestionQuiz questionQuiz) {
		this.questionQuizRepository.delete(questionQuiz);
	}
	
	public List<QuestionQuiz> getQuestionQuiz(){
		return this.questionQuizRepository.findAll();
	}
	
}
