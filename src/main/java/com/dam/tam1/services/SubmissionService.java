package com.dam.tam1.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dam.tam1.entities.HiringProcess;
import com.dam.tam1.entities.Submission;
import com.dam.tam1.repositories.HiringRepository;
import com.dam.tam1.repositories.SubmissionRepository;

@Service 
public class SubmissionService {

	private SubmissionRepository submissionRepository;

	private HiringRepository hiringRepository;
	
	private Integer nextID;
	
	public Long getNextID() {
		Long nextIDL  = Long.valueOf(nextID);
		nextIDL++;
		return nextIDL;
	}
	
//	@Autowired
//	public void setSubmissionRepository(SubmissionRepository submissionRepository) {
//		this.submissionRepository = submissionRepository;
//	}
//	
	public Submission saveSubmission(Submission submission) {
		return this.submissionRepository.save(submission);
	}
	
	@Autowired
	public SubmissionService(SubmissionRepository submissionRepository, HiringRepository hiringRepository) {
		super();
		this.submissionRepository = submissionRepository;
		this.hiringRepository = hiringRepository;
	}
	
	public void updateSubmission(Submission submission) {
		Optional<Submission> submissionFound = this.submissionRepository.findById(submission.getId());
		Submission submiss = submissionFound.get();
		
		if(!submiss.equals(null)) {
			submiss.setMention(submission.getMention());
			this.submissionRepository.save(submiss);
		}
	}



	public void deleteSubmission(Submission submission) {
		this.submissionRepository.delete(submission);
	}
	
	public Submission getSubmissionById(Integer id) {
		Long longID = Long.valueOf(id);
		Submission submissionFound = this.submissionRepository.findById(longID).get();
		List<HiringProcess> hiringProcesses = this.hiringRepository.findAll();
		for(HiringProcess hiringProcess: hiringProcesses) {
			if(hiringProcess.getSubmission().equals(submissionFound)) {
				submissionFound.getAdditionalInformation().put("hiring_id", hiringProcess.getHiring_id().toString());
				submissionFound.getAdditionalInformation().put("candidate_situation", hiringProcess.getCandidateSituation().toString());
				submissionFound.getAdditionalInformation().put("hiring_mentions", hiringProcess.getMentions());
			}
		}
		
		return submissionFound;
	}
	
	public List<Submission> getAllSubmissions(){
		List<HiringProcess> hiringProcesses = this.hiringRepository.findAll();
		List<Submission> submissions = this.submissionRepository.findAll();
		for(Submission submission: submissions) {
			for(HiringProcess hiringProcess: hiringProcesses) {
				if(hiringProcess.getSubmission() == submission) {
					submission.getAdditionalInformation().put("hiring_id", hiringProcess.getHiring_id().toString());
					submission.getAdditionalInformation().put("candidate_situation", hiringProcess.getCandidateSituation().toString());
					submission.getAdditionalInformation().put("hiring_mentions", hiringProcess.getMentions());
					submission.getAdditionalInformation().put("candidate_id", hiringProcess.getSubmission().getCandidate().getCandidate_id());
				}
			}
		}
		
		return this.submissionRepository.findAll();
	}
	
	public void deleteSubmissionById(Integer id) {
		Long long_id = Long.valueOf(id);
		this.submissionRepository.deleteById(long_id);
	}
	
	public void deleteSubmission(Integer id) {
		Long long_id = Long.valueOf(id);
		Submission submissionFound = this.submissionRepository.findById(long_id).get();
		this.submissionRepository.delete(submissionFound);
	}
	
}
