package com.dam.tam1.services;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dam.tam1.entities.Candidate;
import com.dam.tam1.entities.CandidateSituation;
import com.dam.tam1.entities.CommunicationChannel;
import com.dam.tam1.entities.HiringProcess;
import com.dam.tam1.entities.Interview;
import com.dam.tam1.entities.InterviewParticipant;
import com.dam.tam1.entities.Position;
import com.dam.tam1.entities.Submission;
import com.dam.tam1.utils.DateUtils4J8API;

@Service
public class HiringFlowServiceImpl implements IHiringFlowService {
	
	@Autowired
	private InterviewService interviewService;
	
	@Autowired
	private HiringService hiringService;
	
	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private SubmissionService submissionService;
	
	@Autowired
	private CandidateService candidateService;
	
	@Autowired
	private PositionService positionService;
	
	@Override
	public void entrySubmissionWithCandidate(Submission submission) {
		LocalDateTime entryDate = LocalDateTime.now();
		
		System.out.println(entryDate);
		submission.setDateOfSubmission(DateUtils4J8API.asDate(entryDate));
		
		HiringProcess hiringProcess = new HiringProcess(null, null, submission, CandidateSituation.Waiting, null, null);
		submission.setHiring(hiringProcess);
		
		this.submissionService.saveSubmission(submission);
	}

	@Override
	public Interview scheduleInterviewWithDetails(HiringProcess hiringProcess, Date startDate, CommunicationChannel comm) {
		if (hiringProcess.getCandidateSituation().equals(CandidateSituation.ReadyForInterview)
				&& startDate != null) {

			LocalDateTime interviewScheduledDate = DateUtils4J8API.asLocalDateTime(startDate);
			
			Interview scheduledInterview = new Interview(null, DateUtils4J8API.asDate(interviewScheduledDate), comm, hiringProcess, null, null, null);

			hiringProcess.setInterview(scheduledInterview);
			this.notificationService.notificationOnInterview(hiringProcess);
			
			return this.interviewService.saveInterview(scheduledInterview);
			
		} else if (hiringProcess.getCandidateSituation().equals(CandidateSituation.ReadyForInterview)
				&& startDate == null) {

			LocalDateTime interviewScheduledDate = LocalDateTime.now().plusWeeks(2);

			Interview scheduledInterview = new Interview(null, DateUtils4J8API.asDate(interviewScheduledDate), comm, hiringProcess, null, null, null);
			
			hiringProcess.setInterview(scheduledInterview);
			this.notificationService.notificationOnInterview(hiringProcess);
			
			return this.interviewService.saveInterview(scheduledInterview);

		}
		
		return null;
	}

	@Override
	public List<Submission> getSubmissions() {
		return this.submissionService.getAllSubmissions();
	}

	@Override
	public void withdrawSubmission(Integer id) {
		this.submissionService.deleteSubmissionById(id);
	}

	@Override
	public HiringProcess entrySubmissionWithCandidate(Integer candidateId, Integer positionId) {
		
		Candidate candidate = this.candidateService.getCandidateById(candidateId);
		
		Position position = this.positionService.getPositionById(positionId);
		
		LocalDateTime entryDate = LocalDateTime.now();
		
		Submission submission = new Submission(null, DateUtils4J8API.asDate(entryDate), null, position, candidate, null, null);
		
		HiringProcess hiringProcess = new HiringProcess(null, null, submission, CandidateSituation.Waiting, null, null);
		
		this.changeCandidateStatus(hiringProcess);
		return this.hiringService.saveHiring(hiringProcess);
	}

	@Override
	public Interview scheduleSimpleInterview(Interview interview) {
		
		if(interview.getHiring().getCandidateSituation() == CandidateSituation.ReadyForInterview && interview.getInterviewDate() != null) {
			if(interview.getHiring().getInterview() != null) {
				System.out.println("Is here");
				HiringProcess hiring = this.hiringService.getHiringProcessById(interview.getHiring().getHiring_id().intValue());
				Interview interviewToBeRemoved = hiring.getInterview();
				this.interviewService.deleteInterviewById(interviewToBeRemoved.getInterview_id().intValue());
				
				interview.getHiring().setInterview(interview);
				
				Interview interviewNotification = this.hiringService.updateHiringProcessWithInterview(interview.getHiring());
				this.notificationService.notificationOnInterview(interviewNotification.getHiring());
//				hiring.setInterview(interview);
//				this.hiringService.updateHiringProcess(hiring);
//				return this.interviewService.saveInterview(interview);
				
				return interviewNotification;
			} else {
				interview.getHiring().setInterview(interview);
				Interview interviewNotification = this.hiringService.updateHiringProcessWithInterview(interview.getHiring());
				this.notificationService.notificationOnInterview(interviewNotification.getHiring());
				
				return interviewNotification;
			}
		} else if (interview.getHiring().getCandidateSituation() == CandidateSituation.ReadyForInterview && interview.getInterviewDate() == null) {
			LocalDateTime interviewScheduledDate = LocalDateTime.now().plusWeeks(2);
			interview.setInterviewDate(DateUtils4J8API.asDate(interviewScheduledDate));
			interview.getHiring().setInterview(interview);
			
			Interview interviewNotification = this.hiringService.updateHiringProcessWithInterview(interview.getHiring());
			this.notificationService.notificationOnInterview(interviewNotification.getHiring());
			return interviewNotification;
		}
		return null;
	}

	@Override
	public void allocateParticipantToInterview(InterviewParticipant interviewParticipant) {
		
	}

	@Override
	public void changeCandidateStatus(HiringProcess hiringProcess) {
		this.notificationService.notificationOnStatusChange(hiringProcess);
	}


}
