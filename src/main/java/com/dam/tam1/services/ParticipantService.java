package com.dam.tam1.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dam.tam1.entities.Participant;
import com.dam.tam1.repositories.ParticipantRepository;

@Service
public class ParticipantService {

	private ParticipantRepository participantRepository;
	
	@Autowired
	public ParticipantService(ParticipantRepository participantRepo) {
		this.participantRepository = participantRepo;
	}
	
	public Participant saveParticipant(Participant participant) {
		return this.participantRepository.save(participant);		
	}
	
	public Participant updateParticipant(Participant participant) {
		return this.participantRepository.save(participant);
	}
	
	public void deleteParticipant(Participant participant) {
		this.participantRepository.delete(participant);
	}
	
	public void deleteParticipantById(Integer id) {
		Long long_id = Long.valueOf(id);
		this.participantRepository.deleteById(long_id);
	}
	
	public Participant getParticipantById(Integer id) {
		Long participantID = Long.valueOf(id);
		return this.participantRepository.findById(participantID).get();
	}
	
	public List<Participant> getAllParticipants(){
		return this.participantRepository.findAll();
	}
}
