package com.dam.tam1.services;

import java.util.List;
import java.util.ListIterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dam.tam1.entities.Candidate;
import com.dam.tam1.repositories.CandidateRepository;

@Service
public class CandidateService {

	private CandidateRepository candidateRepository;

	@Autowired
	public CandidateService(CandidateRepository candidateRepository) {
		this.candidateRepository = candidateRepository;
	}
	
	public Candidate saveCandidate(Candidate candidate) {
		return this.candidateRepository.save(candidate);
	}
	
	public Candidate updateCandidate(Candidate candidate) {
		return this.candidateRepository.save(candidate);
	}
	
	public void deleteCandidate(Integer id) {
		Long long_id = Long.valueOf(id);
		this.candidateRepository.deleteById(long_id);
	}
	
	public Candidate getCandidateById(Integer id) {
		Long long_id = Long.valueOf(id);
		Candidate existingCandidate = this.candidateRepository.findById(long_id).get();
		return existingCandidate;
	}
	
	public Candidate getCandidateByName(String firstName, String lastName) {
		List<Candidate> existingCandidates = this.candidateRepository.findAll();
		
		if(!existingCandidates.equals(null) || !existingCandidates.isEmpty()) {
			ListIterator<Candidate> candidateIterator = existingCandidates.listIterator();
			while(candidateIterator.hasNext()) {
				Candidate c = candidateIterator.next();
				if(c.getFirstName().equals(firstName) && c.getLastName().equals(lastName)) {
					return c;
				} else {
					System.out.println("No candidate found!");
				}
			}
		}
		
		return null;
	}
	
	public List<Candidate> getAllCandidates() {
		return this.candidateRepository.findAll();
	}
	
	
}
