package com.dam.tam1.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dam.tam1.entities.Answer;
import com.dam.tam1.entities.Question;
import com.dam.tam1.repositories.AnswerRepository;
import com.dam.tam1.repositories.QuestionRepository;

@Service
public class QuestionService {

	private QuestionRepository questionRepository;
	
	private AnswerRepository answerRepository;

	@Autowired
	public void setQuestionRepository(QuestionRepository questionRepository, AnswerRepository answerRepository) {
		this.questionRepository = questionRepository;
		this.answerRepository = answerRepository;
	}
	
	public Question saveQuestion(Question question) {
		return this.questionRepository.save(question);
	}
	
	public void updateQuestion(Question question) {
		Question questionFound = this.questionRepository.findById(question.getQuestion_id()).get();
		questionFound.setQuestion_body(question.getQuestion_body());
		questionFound.setPosition(question.getPosition());
		questionFound.setCorrectAnswer(question.getCorrectAnswer());
		this.questionRepository.save(questionFound);
	}
	
	public void deleteQuestion(Integer id) {
		Long long_id = Long.valueOf(id);
		this.questionRepository.deleteById(long_id);
	}
	
	public Question getQuestionById(Integer id) {
		Long questionID = Long.valueOf(id);
		return this.questionRepository.findById(questionID).get();
	}
	
	public List<Question> getAllQuestions(){
		return this.questionRepository.findAll();
	}
	
	public Collection<Answer> getAllAnswersForQuestion(Integer id){
		List<Answer> answers = this.answerRepository.findAll();
		
		Long question_id = Long.valueOf(id);
		Question questionFound = this.questionRepository.findById(question_id).get();
		
		Collection<Answer> answersFound = new ArrayList<>();
		
		for(Answer answer: answers) {
			if(answer.getQuestion() == questionFound) {
				answersFound.add(answer);
			}
		}
		
		return answersFound;
	}
	
}
