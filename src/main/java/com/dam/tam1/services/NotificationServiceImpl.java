package com.dam.tam1.services;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;

import com.dam.tam1.entities.Candidate;
import com.dam.tam1.entities.HiringProcess;

@Service
public class NotificationServiceImpl implements NotificationService {

	@Override
	public void notificationOnInterview(HiringProcess hiringProcess) {
		
		String candidateEmail = hiringProcess.getSubmission().getCandidate().getEmailAddress();

		String adminEmail = "recruiting@example.com";
		final String adminUsername = "c66c7a99de9d79";
		final String adminPassword = "39da7e6360b986";
		
		String serviceProviderHost = "smtp.mailtrap.io";

		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.host", serviceProviderHost);
		properties.put("mail.smtp.port", "2525");
		
		Session session = Session.getInstance(properties, new Authenticator() {

			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(adminUsername, adminPassword);
			}
		}); 
		
		try {
            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(adminEmail));
            message.setRecipients(Message.RecipientType.TO,
                                     InternetAddress.parse(candidateEmail));

            message.setSubject("Ablahama Recruiting: Interview details");
            message.setText("As we have discussed earlier, " + hiringProcess.getSubmission().getCandidate().getLastName() 
            			+  " you have been scheduled for the interview on the date of " + hiringProcess.getInterview().getInterviewDate().toString());
            Transport.send(message);

            System.out.println("Notification sent succesfully");
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    
	@Override
	public void notificationOnStatusChange(HiringProcess hiringProcess) {
		Candidate candidate = hiringProcess.getSubmission().getCandidate();
    	
    	String candidateTo = candidate.getEmailAddress();

        String senderFrom = "recruiting@example.com";
        final String senderUsername = "c66c7a99de9d79";
        final String senderPassword = "39da7e6360b986";

        String serviceProviderHost = "smtp.mailtrap.io";

        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.host", serviceProviderHost);
        properties.put("mail.smtp.port", "2525");

        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderUsername, senderPassword);
            }
        });

        String candidateSituation = hiringProcess.getCandidateSituation().toString();
        
        
        if (!(candidateSituation.equals("Waiting") || candidateSituation.equals("UnderReview") 
        		|| candidateSituation.equals("WaitingForFinalResponse"))) {
			
        	try {
        		
        		System.out.println();
                Message message = new MimeMessage(session);

                message.setFrom(new InternetAddress(senderFrom));
                message.setRecipients(Message.RecipientType.TO,
                                         InternetAddress.parse(candidateTo));

                message.setSubject("Ablahama recruiting: Hiring status changed!");
                message.setContent("Hello, " + "<b>" + hiringProcess.getSubmission().getCandidate().getLastName() + "</b>" + " you are currently on the state "
                		+ candidateSituation, "text/html");
                Transport.send(message);
                

                System.out.println("Message sent succesfully");
            } catch (AddressException e) {
                e.printStackTrace();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
		}
	}

}
