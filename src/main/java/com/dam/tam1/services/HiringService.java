package com.dam.tam1.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dam.tam1.entities.CandidateSituation;
import com.dam.tam1.entities.HiringProcess;
import com.dam.tam1.entities.Interview;
import com.dam.tam1.repositories.HiringRepository;

@Service
public class HiringService {

	private HiringRepository hiringRepository;
	
	private IHiringFlowService hiringService;
	
	private Integer nextID = 0;
	
	public Long getNextID() {
		Long nextIDL = Long.valueOf(nextID);
		nextIDL++;
		return nextIDL;
	}

	@Autowired
	public void setHiringRepository(HiringRepository hiringRepository) {
		this.hiringRepository = hiringRepository;
	}

	@Autowired
	public void setHiringService(IHiringFlowService hiringService) {
		this.hiringService = hiringService;
	}

	public HiringProcess saveHiring(HiringProcess hiringProcess) {
		hiringProcess.setCandidateSituation(CandidateSituation.Waiting);
		return this.hiringRepository.save(hiringProcess);
	}
	
	public void updateHiringProcess(HiringProcess hiringProcess) {
		HiringProcess updatedHiring = this.getHiringProcessById(hiringProcess.getHiring_id().intValue());
		updatedHiring.setMentions(hiringProcess.getMentions());
		updatedHiring.setCandidateSituation(hiringProcess.getCandidateSituation());
		this.hiringService.changeCandidateStatus(updatedHiring);
		this.hiringRepository.save(updatedHiring);
	}
	
	public Interview updateHiringProcessWithInterview(HiringProcess hiringProcess) {
		return this.hiringRepository.save(hiringProcess).getInterview();
	}
	
	public void deleteHiringProcess(HiringProcess hiringProcess) {
		this.hiringRepository.delete(hiringProcess);
	}
	
	public void deleteHiringProcessById(Integer id) {
		Long long_id = Long.valueOf(id);
		this.hiringRepository.deleteById(long_id);
	}
	
	public HiringProcess getHiringProcessById(Integer id) {
		Long hiringID = Long.valueOf(id);
		
		try {
			Optional<HiringProcess> hiringProcessFound = this.hiringRepository.findById(hiringID);
			HiringProcess hiringProcess = hiringProcessFound.get();
			
			if(hiringProcess.getInterview() == null || hiringProcess.getInterview().getInterviewDate() == null) {
				hiringProcess.getAdditionalInformation().put("firstName", hiringProcess.getSubmission().getCandidate().getFirstName());
				hiringProcess.getAdditionalInformation().put("positionName", hiringProcess.getSubmission().getAppliedPosition().getPositionName());
			} else {
				hiringProcess.getAdditionalInformation().put("interviewDate", hiringProcess.getInterview().getInterviewDate().toString());
				hiringProcess.getAdditionalInformation().put("firstName", hiringProcess.getSubmission().getCandidate().getFirstName());
				hiringProcess.getAdditionalInformation().put("positionName", hiringProcess.getSubmission().getAppliedPosition().getPositionName());
			}
			
			return hiringProcess;
			
		} catch(Exception e) { e.printStackTrace(); }
		
		return null;
	}
	
	public List<HiringProcess> getAllHiringProcesses(){
		List<HiringProcess> hiringProcesses = this.hiringRepository.findAll();
		for(HiringProcess hp : hiringProcesses) {
			if(hp.getInterview() == null || hp.getInterview().getInterviewDate() == null) {
				hp.getAdditionalInformation().put("firstName", hp.getSubmission().getCandidate().getFirstName());
				hp.getAdditionalInformation().put("lastName", hp.getSubmission().getCandidate().getLastName());
				hp.getAdditionalInformation().put("positionName", hp.getSubmission().getAppliedPosition().getPositionName());
				hp.getAdditionalInformation().put("submission_id", hp.getSubmission().getId());
				hp.getAdditionalInformation().put("submission_date", hp.getSubmission().getDateOfSubmission());
			} else {
				hp.getAdditionalInformation().put("interviewDate", hp.getInterview().getInterviewDate().toString());
				hp.getAdditionalInformation().put("firstName", hp.getSubmission().getCandidate().getFirstName());
				hp.getAdditionalInformation().put("lastName", hp.getSubmission().getCandidate().getLastName());
				hp.getAdditionalInformation().put("positionName", hp.getSubmission().getAppliedPosition().getPositionName());
				hp.getAdditionalInformation().put("submission_id", hp.getSubmission().getId());
				hp.getAdditionalInformation().put("submission_date", hp.getSubmission().getDateOfSubmission());
			}
		}
		
		return hiringProcesses;
	}
	
}
