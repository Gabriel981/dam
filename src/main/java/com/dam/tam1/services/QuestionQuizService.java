package com.dam.tam1.services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dam.tam1.entities.QuestionQuiz;
import com.dam.tam1.repositories.QuestionQuizRepository;

@Service
public class QuestionQuizService {

	private QuestionQuizRepository questionQuizRepository;

	@Autowired
	public QuestionQuizService(QuestionQuizRepository questionQuizRepository) {
		super();
		this.questionQuizRepository = questionQuizRepository;
	}
	
	public Collection<QuestionQuiz> getAllQuestionsQuizes(){
		return this.questionQuizRepository.findAll();
	}
	
	
}
