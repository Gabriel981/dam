package com.dam.tam1.services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dam.tam1.entities.Interview;
import com.dam.tam1.entities.InterviewParticipant;
import com.dam.tam1.entities.InterviewParticipantPk;
import com.dam.tam1.entities.Participant;
import com.dam.tam1.repositories.InterviewParticipantRepository;

@Service
public class InterviewParticipantService {

	private InterviewParticipantRepository repository;
	
	@Autowired
	public InterviewParticipantService(InterviewParticipantRepository repository) {
		this.repository = repository;
	}
	
	public InterviewParticipant saveInterviewParticipant(InterviewParticipant interviewParticipant) {
		interviewParticipant.setInterviewParticipantPk(new InterviewParticipantPk(interviewParticipant.getInterview().getInterview_id(), interviewParticipant.getParticipant().getEmployee_id()));
		return this.repository.save(interviewParticipant);
	}
	
	public Collection<InterviewParticipant> getAllParticipantsOnInterview(){
		Collection<InterviewParticipant> interview_participants =  this.repository.findAll();
		for(InterviewParticipant ip: interview_participants) {
			if(ip.getInterview()!=null && ip.getParticipant()!= null) {
				ip.getAdditionalInformation().put("interview_id", ip.getInterview().getInterview_id());
				ip.getAdditionalInformation().put("participant_id", ip.getParticipant().getEmployee_id());
			}
		}
		
		return interview_participants;
	}
	
	public Collection<Interview> getInterviewsByParticipantId(Integer id){
		Collection<InterviewParticipant> participantsByInterview = this.repository.findAll();
		Long participant_id = Long.valueOf(id);
		
		Collection<Interview> interviews = new ArrayList<>();
		for(InterviewParticipant ip: participantsByInterview) {
			if(ip.getParticipant().getEmployee_id() == participant_id) {
				interviews.add(ip.getInterview());
			}
		}
		
		return interviews;
	}
	
	public Collection<Participant> getParticipantsByInterviewId(Integer id){
		Collection<InterviewParticipant> interviewsByParticipant = this.repository.findAll();
		Long interview_id = Long.valueOf(id);
		
		Collection<Participant> participants = new ArrayList<>();
		for(InterviewParticipant ip: interviewsByParticipant) {
			if(ip.getInterview().getInterview_id() == interview_id) {
				participants.add(ip.getParticipant());
			}
		}
		
		return participants;
	}
	
	public void deleteInterviewParticipantById(InterviewParticipantPk id) {
		this.repository.deleteById(id);
	}
	
	
}
