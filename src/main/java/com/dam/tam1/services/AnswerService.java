package com.dam.tam1.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dam.tam1.entities.Answer;
import com.dam.tam1.repositories.AnswerRepository;

@Service
public class AnswerService {

	private AnswerRepository answerRepository;

	@Autowired
	public void setAnswerRepository(AnswerRepository answerRepository) {
		this.answerRepository = answerRepository;
	}
	
	public Answer saveAnswer(Answer answer) {
		return this.answerRepository.save(answer);
	}
	
	public void updateAnswer(Answer answer) {
		this.answerRepository.save(answer);
	}
	
	public void deleteAnswerById(Integer id) {
		Long long_id = Long.valueOf(id);
		this.answerRepository.deleteById(long_id);
	}
	
	
	public Answer getAnswerById(Integer id) {
		Long answerID = Long.valueOf(id);
		return this.answerRepository.findById(answerID).get();
	}
	
	public List<Answer> getAllAnswers(){
		return this.answerRepository.findAll();
	}
}
