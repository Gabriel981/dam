package com.dam.tam1.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dam.tam1.entities.Position;
import com.dam.tam1.repositories.PositionRepository;

@Service
public class PositionService {

	private PositionRepository positionRepository;
	
	private Integer nextID = 0;

	public Long getNextID() {
		Long nextIDL = Long.valueOf(nextID);
		nextIDL++;
		return nextIDL;
	}

	@Autowired
	public void setPositionRepository(PositionRepository positionRepository) {
		this.positionRepository = positionRepository;
	}
	
	public Position savePosition(Position position) {
		return this.positionRepository.save(position);
	}
	
	public Position updatePosition(Position position) {
		Position positionFound = this.positionRepository.findById(position.getPosition_id()).get();
		positionFound.setPositionName(position.getPositionName());
		positionFound.setShortDescription(position.getShortDescription());
		positionFound.setNo_available_positions(position.getNo_available_positions());
		positionFound.setIsAvailable(position.getIsAvailable());
		return this.positionRepository.save(positionFound);
	}
	
	public void deletePosition(Integer id) {
		Long long_id = Long.valueOf(id);
		this.positionRepository.deleteById(long_id);;
	}
	
	public Position getPositionById(Integer id) {
		Long positionID = Long.valueOf(id);
		Optional<Position> positionFound = this.positionRepository.findById(positionID);
		Position position = positionFound.get();
		return position;
	}
	
	public List<Position> getAllPositions(){
		return this.positionRepository.findAll();
	}
}
