//package com.dam.tam1.domain.services;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.ListIterator;
//
//import org.aspectj.lang.annotation.Before;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import com.dam.tam1.entities.Answer;
//import com.dam.tam1.entities.Candidate;
//import com.dam.tam1.entities.CandidateSituation;
//import com.dam.tam1.entities.CommunicationChannel;
//import com.dam.tam1.entities.HiringProcess;
//import com.dam.tam1.entities.Interview;
//import com.dam.tam1.entities.InterviewParticipant;
//import com.dam.tam1.entities.InterviewParticipantPk;
//import com.dam.tam1.entities.Participant;
//import com.dam.tam1.entities.Position;
//import com.dam.tam1.entities.Question;
//import com.dam.tam1.entities.QuestionQuiz;
//import com.dam.tam1.entities.QuestionQuizPk;
//import com.dam.tam1.entities.Quiz;
//import com.dam.tam1.services.AnswerService;
//import com.dam.tam1.services.CandidateService;
//import com.dam.tam1.services.HiringService;
//import com.dam.tam1.services.IHiringFlowService;
//import com.dam.tam1.services.InterviewService;
//import com.dam.tam1.services.ParticipantService;
//import com.dam.tam1.services.PositionService;
//import com.dam.tam1.services.QuestionService;
//import com.dam.tam1.services.QuizService;
//import com.dam.tam1.utils.DateUtils4J8API;
//
//@SpringBootTest
//public class HiringFlowTest {
//
//	//Expected values
//	private final Long CANDIDATE_ID = 1L;
//	private final String FIRST_NAME = "Vasile";
//	private final String LAST_NAME = "Ion";
//	
//	@Autowired
//	private CandidateService candidateService;
//	
//	@Autowired
//	private PositionService positionService;
//	
//	@Autowired
//	private IHiringFlowService iHiring;
//	
//	@Autowired
//	private HiringService hiringService;
//	
//	@Autowired
//	private InterviewService interviewService;
//	
//	@Autowired
//	private ParticipantService participantService;
//	
//	@Autowired
//	private QuizService quizService;
//	
//	@Autowired
//	private QuestionService questionService;
//	
//	@Autowired
//	private AnswerService answerService;
//	
//	@Before(value = "")
//	public Candidate initPersistence() {
//		Candidate candidate = new Candidate(null, "Vasile", "Ion", null, null, null, null, null);
//		return this.candidateService.saveCandidate(candidate);
//	}
//	
//	/*
//	 * @Before(value="") public Position initPersistenceP() { Position position =
//	 * new Position(null, "Position 1", null, null); return
//	 * this.positionService.savePosition(position); }
//	 */
//	
//	public Participant initPersistencePa(){
//		Participant participant = new Participant(null, "First name ", "Last name", "adress", "firstemployee@mail.com", null, null, null, null, null);
//		return this.participantService.saveParticipant(participant);
//	}
//	
//	public Quiz initPersistenceQuiz(){
//		Position position = this.positionService.getPositionById(1);
//		Quiz quiz = new Quiz(null, null, null, position, null);
//		return this.quizService.saveQuiz(quiz);
//	}
//
//	public Question questionPersistence() {
//		Position position = this.positionService.getPositionById(1);
//	
//		Question question = new Question(null, "Ceva", position);
//		return this.questionService.saveQuestion(question);
//	}
//	
//	public Answer answerPersistence(Question question) {
//		Answer answer = new Answer(null, question, "Corp text 1");
//		return this.answerService.saveAnswer(answer);
//	}
//	
////	@Test
////	public void testValuesPersistence() 
////	{
////		Candidate candidate = initPersistence();
////		assertEquals(CANDIDATE_ID, candidate.getCandidate_id());
////		assertEquals(FIRST_NAME, candidate.getFirstName());
////		assertEquals(LAST_NAME, candidate.getLastName());
////	}
//
//	
//	@Test 
//	public void testHiringFlow() { 
//		
////		initPersistenceP(); 
////		initPersistence();
//		Candidate candidate = this.candidateService.getCandidateById(1); 
//		Position position = this.positionService.getPositionById(1);
//		
//		// // this.iHiring.scheduleInterviewWithoutDetails(this.hiringService.getHiringProcessById(1), null);
//
//		candidate.setEmailAddress("candidate@example.com"); 
//		Candidate newCandidate = this.candidateService.updateCandidate(candidate);
//		 
////		 this.iHiring.entrySubmissionWithCandidate(candidate, position); 
//		 
//		 HiringProcess hiringProcess = this.hiringService.getHiringProcessById(1);
//		 
//		 hiringProcess.setCandidateSituation(CandidateSituation.ReadyForInterview);
//		 
//		 this.hiringService.updateHiringProcess(hiringProcess);
//		
//		 
//		 this.iHiring.scheduleInterviewWithDetails(hiringProcess, null, CommunicationChannel.ONLINE); 
//	 }
//	 
//	
////	@Test
////	public void testHiringFlowParticipant() {
////		
////		//New Participant + allocation of Participant to an Interview;
////		Participant p = initPersistencePa();
////		Participant pa = this.participantService.getParticipantById(1);
////		Interview i = this.interviewService.getInterviewById(1);
////		InterviewParticipantPk pk = new InterviewParticipantPk();
////		pk.setInterviewId(i.getInterview_id());
////		pk.setParticipantId(pa.getEmployee_id());
////		InterviewParticipant ipa = new InterviewParticipant();
////		ipa.setInterviewParticipantPk(pk);
////		this.interviewService.saveParticipantInterview(ipa);
////		
////		LocalDateTime localDate = LocalDateTime.now();
////		ipa.setDateOfAllocation(DateUtils4J8API.asDate(localDate));
////		this.interviewService.updateParticipantInterview(ipa);
////	}
//	
//
//	
////	@Test
////	public void testHiringFlowQuiz() {
//////		Quiz quiz = initPersistenceQuiz();
////		Quiz quiz = this.quizService.getQuizById(1);
////	
////		Question question = this.questionService.getQuestionById(1);
//////		Answer answer = answerPersistence(question);
////
////		QuestionQuizPk questionQuizPk1 = new QuestionQuizPk();
////		questionQuizPk1.setQuizId(quiz.getQuiz_id());
////		questionQuizPk1.setQuestionId(question.getQuestion_id());
////				
////		QuestionQuiz questionQuiz1 = new QuestionQuiz(questionQuizPk1);
////		
////		this.quizService.saveQuestionQuiz(questionQuiz1);
////	}
////	
//	
//	
//	
//}
